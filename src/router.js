import Vue from "vue";
import Router from "vue-router";

import { isSignedIn } from "@/services/auth";

import Login from "@/components/Login/Login.vue";
import Dashboard from "@/components/Dashboard/Dashboard.vue";
import RegisterAuthor from "@/components/Register/Author/RegisterAuthor.vue"
import RegisterAssisted from "@/components/Register/Assisted/RegisterAssisted.vue"
import RegisterUser from "@/components/Register/UserForm/UserForm.vue"
import EditAuthor from "@/components/Table/Author/EditAuthor.vue"
import EditAssisted from "@/components/Table/Assisted/EditAssisted.vue"
import EditUser from "@/components/Table/User/EditUser.vue"
import RegisterVehicle from "@/components/Register/Vehicle/RegisterVehicle.vue"
import AssistedTable from "@/components/Table/Assisted/AssistedTable.vue"
import ProtectiveTable from "@/components/Table/Measure/MeasureTable.vue"
import EditMeasure from "@/components/Table/Measure/EditMeasure.vue"
import AuthorTable from "@/components/Table/Author/AuthorTable.vue"
import VehicleTable from "@/components/Table/Vehicle/VehicleTable.vue"
import EditVehicle from "@/components/Register/Vehicle/EditVehicle.vue"
import UserTable from "@/components/Table/User/UserTable.vue"
import Map from "@/components/Map/Index.vue";
/* import Password from "@/components/ChangePassword/index.vue" */
import RegisterMeasure from "@/components/Register/ProtectiveMeasure/RegisterMeasure.vue"
import GeneralInfo from "@/components/GeneralInfo/GeneralInfo.vue"
import ChangePass from "@/components/ChangePass/Change.vue"
import AlertPopup from "@/components/Map/AlertPopup.vue"


Vue.use(Router);

const loggedInGuard = (to, from, next) => {
  if (isSignedIn()) {
    next();
  }
  else next("/");
}


export default new Router({
  mode: "history",
  base: __dirname,
  routes: [
    {
      path: "/",
      name: "Login",
      component: Login,
    },
    {
      path: "/home",
      name: "Home",
      beforeEnter: loggedInGuard,
      component: Dashboard,
    },
    {
      path: "/change",
      name: "Change",
      beforeEnter: loggedInGuard,
      component: ChangePass,
    },
    {
      path: "/register_author",
      beforeEnter: loggedInGuard,
      component: RegisterAuthor
    },
    {
      path: "/register_assisted",
      beforeEnter: loggedInGuard,
      component: RegisterAssisted
    }, 
    {
      path: "/manage_authors",
      beforeEnter: loggedInGuard,
      component: AuthorTable
    },
    {
      path: "/register_vehicle",
      beforeEnter: loggedInGuard,
      component: RegisterVehicle
    },
    {
      path: "/manage_assisteds",
      beforeEnter: loggedInGuard,
      component: AssistedTable
    }, 
    {
      path: "/edit_author",
      beforeEnter: loggedInGuard,
      component: EditAuthor
    },
    {
      path: "/edit_assisted",
      beforeEnter: loggedInGuard,
      component: EditAssisted
    },
    {
      path: "/edit_user",
      beforeEnter: loggedInGuard,
      component: EditUser
    },
    {
      path: "/edit_measure",
      beforeEnter: loggedInGuard,
      component: EditMeasure
    },
    {
      path: "/manage_protectives",
      beforeEnter: loggedInGuard,
      component: ProtectiveTable
    },

    {
      path: "/manage_authors",
      beforeEnter: loggedInGuard,
      component: EditAssisted
    },
    {
      path: "/manage_vehicles",
      beforeEnter: loggedInGuard,
      component: VehicleTable
    },
    {
      path: "/edit_vehicle",
      beforeEnter: loggedInGuard,
      component: EditVehicle
    },
    {
      path: "/manage_users",
      beforeEnter: loggedInGuard,
      component: UserTable
    },
    {
      path: "/register_user",
      beforeEnter: loggedInGuard,
      component: RegisterUser
    },
    {
      path: "/map",
      beforeEnter: loggedInGuard,
      component: Map
    },
    {
      path: "/register_measure",
      beforeEnter: loggedInGuard,
      component: RegisterMeasure
    },
    {
      path: "/general_info",
      beforeEnter: loggedInGuard,
      component: GeneralInfo, 
    },
    {      
      path: "/alertpopup",
      beforeEnter: loggedInGuard,
      component: AlertPopup
    }
  ],
});

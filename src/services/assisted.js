import axios from "axios"

export async function getAssisteds() {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + "admin/assisted", config);
    return response.data
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function update(model, id) {
  try {
    const config = {
      headers: {
        'Authorization': localStorage.getItem("token"),
        'Content-Type': 'multipart/form-data'
      }
    }
    return await axios.put(process.env.VUE_APP_API + `admin/assisted/${id}`, model, config);
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function getOne(id) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + `admin/assisted/${id}`, config);
    return response.data
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function getAssistedPicture(id) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + `admin/assisted/${id}/picture`, config);
    if(response.status != 200)
      return[];
    return response.data;
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return [];
  }
}

export async function deleteAssisted(id) {
  try {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token")
      }
    }
    let response = await axios.delete(process.env.VUE_APP_API + "admin/assisted/" + id, config);
    return response;
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function registerAssisted(payload) {
  try {
    const config = {
      headers: {
        'Authorization': localStorage.getItem("token"),
        'Content-Type': 'multipart/form-data'
      }
    }
    return await axios.post(process.env.VUE_APP_API + "admin/assisted/", payload, config);
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}
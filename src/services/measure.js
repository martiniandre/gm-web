import axios from "axios"

export async function getMeasure() {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + "admin/measure", config);
    return response.data
  } catch (error) {
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}
export async function getByAssisted(assistedId) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + "admin/measure/byAssisted/"+assistedId, config);
    return response.data
  } catch (error) {
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function registerMeasure(measure) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.post(process.env.VUE_APP_API + "admin/measure/", measure, config);
    return response;
  }
  catch (error) {
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function deleteMeasure(measureId) {
  try {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token")
      }
    }
   let response = axios.delete(process.env.VUE_APP_API + "admin/measure/" + measureId, config);
   return response
  } catch (error) {
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function getOne(measureId) {
  try {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token")
      }
    }
   let response = await axios.get(process.env.VUE_APP_API + "admin/measure/" + measureId, config);
   return response.data
   
  } catch (error) {
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}
export async function update(id, data) {
  try {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token")
      }
    }
   let response = await axios.put(process.env.VUE_APP_API + "admin/measure/" + id, data, config);
   return response.data;
   
  } catch (error) {
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

import axios from "axios"

export async function getVehicle(authorId) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    if (authorId) {
      config.params = { authorId };
    }
    let response = await axios.get(process.env.VUE_APP_API + "admin/vehicle", config);
    return response.data
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function registerVehicle(vehicle) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.post(process.env.VUE_APP_API + "admin/vehicle/", vehicle, config);
    return response;
  }
  catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function editVehicle(vehicleId, vehicle) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.put(process.env.VUE_APP_API + `admin/vehicle/${vehicleId}`, vehicle, config);
    return response;
  }
  catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function deleteVehicle(vehicleId) {
  try {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token")
      }
    }
   let response = axios.delete(process.env.VUE_APP_API + "admin/vehicle/" + vehicleId, config);
   return response
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function getOne(vehicleId) {
  try {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token")
      }
    }
   let response = await axios.get(process.env.VUE_APP_API + "admin/vehicle/" + vehicleId, config);
   console.log(response)
   return response.data;
   
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function getByAuthor(authorId) {
  try {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token")
      }
    }
   let response = await axios.get(process.env.VUE_APP_API + "admin/vehicle/byAuthor/" + authorId, config);
   return response.data;
   
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

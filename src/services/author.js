import axios from "axios"

export async function getAuthors() {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + "admin/author", config);
    return response.data
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function getOne(id) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + `admin/author/${id}`, config);
    return response.data
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}
export async function getOneToMeasure(id) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + `admin/author/${id}`, config);
    return response.data
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function getAuthorPicture(id) {
  try {
    const config = {
      headers: { Authorization: localStorage.getItem("token") }
    }
    let response = await axios.get(process.env.VUE_APP_API + `admin/author/`+ id + `/picture`, config);
    console.log(response);
    if(response.status != 200)
      return[];
    return response.data;
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return [];
  }
}

export async function editAuthor(id, data) {
  try {
    const config = {
      headers: {
        'Authorization': localStorage.getItem("token"),
        'Content-Type': 'multipart/form-data'
      }
    }
    return await axios.put(process.env.VUE_APP_API + "admin/author/" + id, data, config);
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}

export async function registerAuthor(author) {

  try {
    const config = {
      headers: {
        'Authorization': localStorage.getItem("token"),
        'Content-Type': 'multipart/form-data'
      }
    }
    let response = await axios.post(process.env.VUE_APP_API + "admin/author/", author, config);
    return response;
  }
  catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }

}

export async function deleteAuthor(id) {
  try {
    const config = {
      headers: {
        Authorization: localStorage.getItem("token")
      }
    }
    let response = await axios.delete(process.env.VUE_APP_API + "admin/author/" + id, config);
    return response;
  } catch (error) {
    console.error({
      status: error.response.status,
      error: Promise.reject(error)
    });
    return {
      status: error.response.status,
      error: Promise.reject(error)
    }
  }
}